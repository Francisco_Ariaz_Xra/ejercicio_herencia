public class Aplicacion {
    public static void main(String[] args) {
        Sobremesa s1 = new Sobremesa();
        Sobremesa s2 = new Sobremesa();
        s1.eslogan();
        s1.codigo();

        s2.precio();
        s2.tipoTorre();

        Portatil p1 = new Portatil();
        Portatil p2 = new Portatil();
        p1.eslogan();
        p1.codigo();

        p2.precio();
        p2.peso();
    }
}